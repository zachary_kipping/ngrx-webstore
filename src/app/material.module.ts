import { NgModule } from '@angular/core';
import {
  MatButtonModule,
  MatCardModule,
  MatSnackBarModule,
  MatFormFieldModule,
  MatInputModule,
  MatToolbarModule,
  MatIconModule,
  MatGridListModule,
  MatDividerModule,
  MatTableModule,
} from '@angular/material';

const MatModules = [
  MatButtonModule,
  MatCardModule,
  MatSnackBarModule,
  MatFormFieldModule,
  MatInputModule,
  MatToolbarModule,
  MatIconModule,
  MatGridListModule,
  MatDividerModule,
  MatTableModule,
]

@NgModule({
  imports: MatModules,
  exports: MatModules,
})
export class MaterialModule {}