import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'browse', pathMatch: 'full' },
  { path: 'browse', loadChildren: './browse/browse.module#BrowseModule' },
  { path: 'details/:id', loadChildren: './details/details.module#DetailsModule' },
  { path: 'cart', loadChildren: './cart/cart.module#CartModule' },
  { path: '**', redirectTo: 'browse' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
