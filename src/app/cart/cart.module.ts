import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CartComponent } from './cart.component';
import { Routes, RouterModule } from '@angular/router';
import { MaterialModule } from '../material.module';

const cartRoutes: Routes = [
  { path: '', component: CartComponent }
]

@NgModule({
  declarations: [
    CartComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule.forChild(cartRoutes)
  ],
})
export class CartModule {}