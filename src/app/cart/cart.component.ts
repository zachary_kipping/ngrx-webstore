import { Component } from '@angular/core';
import { Observable } from 'rxjs';

import { CartItem } from '../app.state';
import { WebStoreService } from '../webstore.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent {
  cart: Observable<CartItem[]>;
  total: Observable<number>;
  displayedColumns: string[] = ['name', 'rating', 'price', 'count', 'action'];

  constructor(private ws: WebStoreService) {
    this.cart = ws.cart;
    this.total = ws.cartTotalCost;
  }

  removeItem(id: string) {
    this.ws.removeItemFromCart(id);
  }
}
