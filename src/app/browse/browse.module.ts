import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BrowseComponent } from './browse.component';
import { RouterModule, Routes } from '@angular/router';
import { MaterialModule } from '../material.module';

const browseRoutes: Routes = [
  { path: '', component: BrowseComponent }
]

@NgModule({
  declarations: [
    BrowseComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(browseRoutes),
    MaterialModule
  ],
})
export class BrowseModule { }