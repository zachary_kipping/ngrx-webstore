import { Component} from '@angular/core';
import { WebStoreService } from '../webstore.service';
import { Observable } from 'rxjs';
import { Item } from '../app.state';

@Component({
  selector: 'app-browse',
  templateUrl: './browse.component.html',
  styleUrls: ['./browse.component.scss']
})
export class BrowseComponent {
  items: Observable<Item[]>;
  auth: Observable<boolean>;

  constructor(private ws: WebStoreService) {
    this.items = ws.items;
    this.auth = ws.auth;
  }

  addItem(item: Item) {
    this.ws.addItemToCart(item);
  }
}
