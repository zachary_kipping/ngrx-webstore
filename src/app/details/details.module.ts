import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DetailsComponent } from './details.component';
import { Routes, RouterModule } from '@angular/router';
import { MaterialModule } from '../material.module';

const detailsRoutes: Routes = [
  { path: '', component: DetailsComponent }
]

@NgModule({
  declarations: [
    DetailsComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule.forChild(detailsRoutes)
  ],
})
export class DetailsModule {}