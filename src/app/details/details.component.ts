import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, combineLatest } from 'rxjs';
import { map, tap, filter } from 'rxjs/operators';

import { WebStoreService } from '../webstore.service';
import { Item } from '../app.state';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent {
  details: Observable<Item>;
  auth: Observable<boolean>;

  constructor(private ws: WebStoreService, route: ActivatedRoute, router: Router) {
    this.details = combineLatest(
      ws.items.pipe(filter(items => items.length > 0)),
      route.paramMap,
      (items, params) => ({ items, params })
    ).pipe(
      map(result => result.items.find(item => item.id === result.params.get('id'))),
      tap(item => {
        if (!item) {
          router.navigate(['browse']);
        }
      }),
      filter(resultIsItem)
    );
    
    this.auth = ws.auth;
  }

  addItem(item?: Item) {
    if (resultIsItem(item)) {
      this.ws.addItemToCart(item);
    }
  }
}

function resultIsItem(foo: Item | undefined): foo is Item {
  return !!foo;
}
