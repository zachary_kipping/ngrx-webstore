import { Component } from '@angular/core';
import { WebStoreService } from './webstore.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  auth: Observable<boolean>;
  cartLength: Observable<number>;

  constructor(private ws: WebStoreService) {
    this.auth = ws.auth;
    this.cartLength = ws.cartLength;
  }

  login() {
    this.ws.login();
  }

  logout() {
    this.ws.logout();
  }
}
