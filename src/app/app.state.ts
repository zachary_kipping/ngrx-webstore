import { Action, createFeatureSelector, createSelector } from '@ngrx/store';

export interface Item {
  id: string;
  name: string;
  price: number;
  description: string;
  rate: number;
  image: string;
}

export interface CartItem {
  item: Item;
  count: number;
}

export interface AppState {
  items: Item[];
  cart: CartItem[];
  auth: boolean;
}

export const getItems = createFeatureSelector<Item[]>('items');
export const getCart = createFeatureSelector<CartItem[]>('cart');
export const getAuth = createFeatureSelector<boolean>('auth');

export const getCartLength = createSelector(getCart, cart => {
  if (cart.length > 0) {
    return cart.map(item => item.count).reduce((prev, curr) => prev + curr);
  } else {
    return 0;
  }
});

export const getCartTotalCost = createSelector(getCart, cart => {
  if (cart.length > 0) {
    const itemTotals = cart.map(obj => obj.item.price * obj.count);
    return itemTotals.reduce((prev, curr) => prev + curr);
  } else {
    return 0;
  }
})

export const STORE_ITEMS_ARRIVED = 'STORE_ITEMS_ARRIVED';
export class StoreItemsArrivedAction {
  readonly type = STORE_ITEMS_ARRIVED;
  constructor(readonly items: Item[]) { }
}

export function ItemsReducer(state: Item[] = [], action: Action): Item[] {
  switch (action.type) {
    case STORE_ITEMS_ARRIVED:
      return [...(action as StoreItemsArrivedAction).items];
    default:
      return state;
  }
}

export const AddItemToCart = 'AddItemToCart';
export class AddItemToCartAction implements Action {
  readonly type = AddItemToCart;
  constructor(readonly item: Item) { }
}

export const RemoveItemFromCart = 'RemoveItemFromCart';
export class RemoveItemFromCartAction implements Action {
  readonly type = RemoveItemFromCart;
  constructor(readonly id: string) { }
}

type CartActions = RemoveItemFromCartAction | AddItemToCartAction;

export function CartReducer(state: CartItem[] = [], action: CartActions): CartItem[] {
  switch (action.type) {
    case AddItemToCart:
      const itemToAdd = action.item;
      const indexInCartAdd = state.findIndex(i => i.item.id === itemToAdd.id);
      const itemInCart = state[indexInCartAdd];
      if (indexInCartAdd >= 0) {
        return [
          ...state.slice(0, indexInCartAdd),
          {...itemInCart, count: itemInCart.count + 1},
          ...state.slice(indexInCartAdd + 1)
        ]
      } else {
        return [...state, { count: 1, item: itemToAdd }];
      }
    case RemoveItemFromCart:
      const idToRemove = action.id;
      const indexInCartRemove = state.findIndex(i => i.item.id === idToRemove);
      if (indexInCartRemove >= 0) {
        const item = state[indexInCartRemove];
        if (item.count > 1) {
          return [
            ...state.slice(0, indexInCartRemove),
            {...item, count: item.count - 1 },
            ...state.slice(indexInCartRemove + 1)
          ];
        } else {
          return state.filter(c => c.item.id !== idToRemove);
        }
      } else {
        return state;
      }
    default:
      return state;
  }
}

export const LOGIN = 'LOGIN';
export class LoginAction {
  readonly type = LOGIN;
}

export const LOGOUT = 'LOGOUT';
export class LogoutAction {
  readonly type = LOGOUT;
}

export function AuthReducer(state: boolean = false, action: Action): boolean {
  switch (action.type) {
    case LOGIN:
      return true;
    case LOGOUT:
      return false;
    default:
      return state;
  }
}
