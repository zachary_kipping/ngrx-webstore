import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppState, ItemsReducer, CartReducer, AuthReducer } from './app.state';
import { WebStoreService } from './webstore.service';
import { MaterialModule } from './material.module';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    MaterialModule,
    StoreModule.forRoot<AppState>({
      items: ItemsReducer,
      cart: CartReducer,
      auth: AuthReducer
    }),
    EffectsModule.forRoot([
      WebStoreService
    ])
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
