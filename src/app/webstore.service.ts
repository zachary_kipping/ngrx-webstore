import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Actions, Effect, ofType, ROOT_EFFECTS_INIT } from '@ngrx/effects';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { switchMap, map, shareReplay } from 'rxjs/operators';

import {
  Item,
  StoreItemsArrivedAction,
  AppState,
  getItems,
  getCart,
  getAuth,
  AddItemToCartAction,
  RemoveItemFromCartAction,
  LoginAction,
  LogoutAction,
  CartItem,
  getCartLength,
  getCartTotalCost
} from './app.state';

const API_URL = 'https://my.api.mockaroo.com/ngrx-webstore-data.json?key=da2b3460';

@Injectable({
  providedIn: 'root'
})
export class WebStoreService {
  items: Observable<Item[]>;
  auth: Observable<boolean>;
  cart: Observable<CartItem[]>;
  cartLength: Observable<number>;
  cartTotalCost: Observable<number>;

  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private http: HttpClient
  ) {
    this.items = this.store.pipe(select(getItems));
    this.auth = this.store.pipe(select(getAuth));
    
    this.cart = this.store.pipe(select(getCart));
    this.cartLength = this.store.pipe(select(getCartLength));
    this.cartTotalCost = this.store.pipe(select(getCartTotalCost));
  }

  @Effect() init$ = this.actions$.pipe(
    ofType(ROOT_EFFECTS_INIT),
    switchMap(() => this.getItems()),
    map(items => new StoreItemsArrivedAction(items))
  );

  getItems(): Observable<Item[]> {
    return this.http.get<Item[]>(API_URL);
  }

  addItemToCart(item: Item): void {
    this.store.dispatch(new AddItemToCartAction(item));
  }

  removeItemFromCart(id: string): void {
    this.store.dispatch(new RemoveItemFromCartAction(id));
  }

  login(): void {
    this.store.dispatch(new LoginAction());
  }

  logout(): void {
    this.store.dispatch(new LogoutAction());
  }
}
